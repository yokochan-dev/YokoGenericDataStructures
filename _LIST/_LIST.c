#include "_LIST.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>

typedef struct _NODE_STRUCT *_LINK;

typedef struct _NODE_STRUCT
{
	void *item;
	_LINK next;
}_NODE;

/*The compare function must return 0 if elements are equal.*/

struct _LIST_STRUCT
{
	_LINK head;
	_LINK tail;
	_LINK sentinel;
	size_t size_item;
	int (*compare)(void *a1, void *a2);
	void (*print)(void *a);
};

/*The function create_node returns a _LINK to a node.
 * Returns:
 * valid pointer to node if succesful;
 * NULL otherwise.
 * Parameters:
 * void *item: item to copy;
 * _LINK next: pointer to node to set as next field;
 * size_t size_item: size in bytes of the item;
 */
static _LINK create_node(void *item, _LINK next, size_t size_item)
{
	_LINK node=malloc(sizeof(_NODE));
	//Check 1: see if node allocation is valid
	if(node==NULL)
		return NULL;
	node->item=malloc(size_item);
	//Check 2: see if item allocation is valid
	if(node->item==NULL)
	{
		free(node);
		return NULL;
	}
	//Copy the item and set the correct next node
	memcpy(node->item,item,size_item);
	node->next=next;
	return node;
}

/*The function _LIST_create creates a list of items of size_item bytes.
 * Returns:
 * pointer to list if succesful;
 * NULL otherwise.
 * Parameters:
 * _size_t size_item: size in bytes of the items;
 * compareElementInList compare: the function used to compare the elements in the list. Can be NULL, but will block some functions;
 * printElementInList print: the function used to print the element in the list. Can be NULL, but will block some functions.
 */
_LIST _LIST_create(size_t size_item, int (*compare)(void *a1, void *a2), void (*print)(void *a))
{
	_LIST list=malloc(sizeof(struct _LIST_STRUCT));
	//Allocate item for sentinel node
	void *item=malloc(size_item);
	if(item==NULL)
		return NULL;
	memset(item,0,size_item);
	//Create the list
	if(list==NULL)
	{
		free(item);
		return NULL;
	}
	list->sentinel=create_node(item,NULL,size_item);
	//If cannot create the node, free the list and return NULL
	if(list->sentinel==NULL)
	{
		free(item);
		free(list);
		return NULL;
	}
	//Set head and tail as sentinel and free item
	list->head=list->sentinel;
	list->tail=list->sentinel;
	list->size_item=size_item;
	list->compare=compare;
	list->print=print;
	free(item);
	return list;
}

/*The function _LIST_add adds a node to the end of the list and updates the tail.
 * The function makes a copy of the element to add to the list.
 * Returns:
 * 1 if the addition was succesful;
 * 0 if the addition was not succesful.
 * Parameters:
 * _LIST list: pointer to list to update;
 * void *item: item to copy into the node;
 * size_t size_item: size in bytes of the item.
 */
_Bool _LIST_add(_LIST list, void *item, size_t size_item)
{
	_LINK new_node;
	//Flag is put to 0 everytime an invalid operation is performed
	_Bool flag=1;
	//Check on correct element size
	if(size_item!=list->size_item)
		flag=0;
	//Check in case user passed an invalid item
	if(item==NULL)
		flag=0;
	//Create the node, check if it is a valid node, if not return
	new_node=create_node(item,list->sentinel,size_item);
	if(new_node==NULL)
		return 0;
	//Case 1: list is empty, head and tail are the same node
	if(list->head==list->sentinel)
	{
		list->head=new_node;
		list->tail=list->head;
	}
	//Case 2: list is not empty, update the tail
	else
	{
		list->tail->next=new_node;
		list->tail=list->tail->next;
	}
	return flag;
}

/*The function _LIST_search returns the index of the item to search.
 * Returns:
 * index of the first node containing the item searched;
 * -1 if not item wasn't found, or illegal, or the list doesn't exist;
 * -2 if the client didn't provide a valid compare function in the _LIST_create.
 * Parameters:
 * _LIST list: list to perform the search on;
 * void *item: item to search.
 */
int _LIST_search(_LIST list, void *item)
{
	_LINK current_node;
	int i;
	if(list==NULL)
		return -1;
	if(list->compare==NULL)
		return -2;
	if(item==NULL)
		return -1;
	for(current_node=list->head,i=0;current_node!=list->sentinel;current_node=current_node->next,i++)
	{
		if(list->compare(item,current_node->item)==0)
			return i;
	}
	return -1;
}

/*The function _LIST_free frees memory occupied by the list.
 * Returns:
 * 1 if deallocation was succesful;
 * 0 if wasn't succesful, or list doesn't exist.
 * Parameters:
 * _LIST list: list to operate on.
 */
_Bool _LIST_free(_LIST list)
{
	if(list==NULL)
		return 0;
	_LINK prev,current;
	for(prev=list->head,current=list->head->next;prev!=list->sentinel;prev=current,current=current->next)
	{
		free(prev->item);
		free(prev);
	}
	free(list->sentinel->item);
	free(list->sentinel);
	free(list);
	return 1;
}

static _Bool clear_node(_LINK node)
{
	free(node->item);
	free(node);
	return 1;
}

/*The function LIST_search_destroy searches an item in the list and deletes it.
 * Returns:
 * 1 in case of succesful deletion;
 * -1 if the item isn't found, or the list doesn't exist;
 * -2 if the compare function wasn't provided.
 * Parameters:
 * _LIST list: the list to operate on;
 * void *item: the item to search.
 */
int _LIST_search_destroy(_LIST list,void *item)
{
	_LINK prev,current_node;
	if(list==NULL || item==NULL)
		return -1;
	if(list->compare==NULL)
		return -2;
	if(list->compare(list->head->item,item)==0)
	{
		prev=list->head->next;
		clear_node(list->head);
		list->head=prev;
		return 1;
	}
	else
	{
		for(current_node=list->head->next,prev=list->head;
			current_node!=list->sentinel;
			prev=current_node,current_node=current_node->next)
		{
			if(list->compare(current_node->item,item)==0)
			{
				//If the item is in the tail, update with new tail
				if(current_node==list->tail)
					list->tail=prev;
				prev->next=prev->next->next;
				clear_node(current_node);
				return 1;
			}
		}
	}
	return -1;
}

/*The function _LIST_search_by_index searches the index-th element in the list and returns it.
 * The function doesn't return a copy of the element but a pointer to the element itself.
 * Returns:
 * pointer to the item contained in the index-th node in the list if succesful;
 * NULL if the index-th position doesn't exist, or the list doesn't exist.
 * Parameters:
 * _LIST list: list to operate on;
 * int index: index of the element to search.
 */
void *_LIST_search_by_index(_LIST list, int index)
{
	_LINK current_node;
	int i;
	if(index<0 || list==NULL)
		return NULL;
	for(current_node=list->head,i=0;current_node!=list->sentinel && i<=index;current_node=current_node->next,i++)
		if(i==index)
			return current_node->item;
	return NULL;
}

/*The function _LIST_search_destroy_by_index searches and destroys the index-th element in the list and returns it.
 * The function returns a copy of the element. 
 * The function allocates enough space to contain the item, deallocation is left to the client.
 * Returns:
 * pointer to the item contained in the index-th node in the list if succesful;
 * NULL if the index-th position doesn't exist, or the list doesn't exist.
 * Parameters:
 * _LIST list: list to operate on;
 * int index: index of the element to search.
 */
void *_LIST_search_destroy_by_index(_LIST list, int index)
{
	_LINK current_node,prev;
	void *item;
	int i;
	//Case 1: index is illegal or list doesn't exist
	if(index<0 || list==NULL)
		return NULL;
	//Case 2: the item is in the head, must update with new head
	if(index==0 && list->head!=list->sentinel)
	{
		current_node=list->head->next;
		item=malloc(list->size_item);
		if(item==NULL)
			return NULL;
		memcpy(item,list->head->item,list->size_item);
		clear_node(list->head);
		list->head=current_node;
		return item;
	}
	//Case 3: index is between head->next and tail
	for(prev=list->head,i=1,current_node=list->head->next;
		current_node!=list->sentinel && i<=index;
		prev=current_node,current_node=current_node->next,i++)
		if(i==index)
		{
			//Update with new tail if the index is the tail
			if(current_node==list->tail)
				list->tail=prev;
			item=malloc(list->size_item);
			if(item==NULL)
				return NULL;
			memcpy(item,current_node->item,list->size_item);
			prev->next=prev->next->next;
			clear_node(current_node);
			return item;
		}
	return NULL;
}

_Bool _LIST_print(_LIST list)
{
	_LINK current_node;
	if(list==NULL || list->print==NULL)
		return 0;
	if(list->head==list->sentinel)
		return 0;
	for(current_node=list->head;current_node!=list->sentinel;current_node=current_node->next)
	{
		list->print(current_node->item);
	}
	return 1;
}
