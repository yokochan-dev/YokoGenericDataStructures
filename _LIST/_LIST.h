#ifndef _LIST_H
#define _LIST_H 1
#include <stddef.h>

typedef struct _LIST_STRUCT *_LIST;

_LIST _LIST_create(size_t size_item, int (*compare)(void *a1,void *a2), void (*print)(void *a));
_Bool _LIST_add(_LIST list, void *item, size_t size_item);
int _LIST_search(_LIST list, void *item);
int _LIST_search_destroy(_LIST list,void *item);
void *_LIST_search_by_index(_LIST list, int index);
void *_LIST_search_destroy_by_index(_LIST list, int index);
void **_LIST_to_array(_LIST list, size_t *n);
void _LIST_sort(_LIST list);
_Bool _LIST_free(_LIST list);
_Bool _LIST_print(_LIST list);
#endif
