#include "_STACK.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct _STACK_STRUCT
{
	void *array;
	int stack_pointer;
	size_t size_single_element;
	size_t allocated_size;
};

static _Bool double_array_size(_STACK stack)
{
	stack->array=realloc(stack->array,(stack->allocated_size)*2);
	if(stack->array==NULL)
		return 0;
	stack->allocated_size*=2;
	return 1;
}

static _Bool is_full(_STACK stack)
{
	if(stack->allocated_size==stack->stack_pointer)
		return 1;
	return 0;
}

_Bool _STACK_push(_STACK stack, void *element, size_t size_element)
{
	if((size_element)!=(stack->size_single_element))
		return 0;
	if(is_full(stack))
		if(!double_array_size(stack))
			return 0;
	memcpy((stack->array)+((stack->stack_pointer)*(stack->size_single_element)),element,size_element);
	stack->stack_pointer++;
	return 1;
}

_STACK _STACK_create(size_t size_single_element)
{
	size_t default_array_size=16;
	_STACK stack;
	stack=(_STACK)malloc(sizeof(struct _STACK_STRUCT));
	stack->size_single_element=size_single_element;
	stack->allocated_size=default_array_size;
	stack->stack_pointer=0;
	if((stack->array=malloc(default_array_size*size_single_element))==NULL)
		return NULL;
	return stack;
}

void _STACK_copy(_STACK stack, void* array_to_copy, size_t array_size)
{
	while(array_size>stack->allocated_size)
		double_array_size(stack);
	memcpy(stack->array+stack->stack_pointer,array_to_copy,array_size*stack->size_single_element);
	stack->stack_pointer=array_size;
	return;
}

_Bool _STACK_is_empty(_STACK stack)
{
	if(stack->stack_pointer==0)
		return 1;
	return 0;
}

void* _STACK_pop(_STACK stack)
{
	if(_STACK_is_empty(stack))
		return NULL;
	stack->stack_pointer--;
	return (stack->array)+((stack->stack_pointer)*(stack->size_single_element));
}

void _STACK_free(_STACK stack)
{
	free(stack->array);
	free(stack);
	return;
}


