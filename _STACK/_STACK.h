#ifndef _STACK_H
#define _STACK_H 1
#include <stddef.h>

typedef struct _STACK_STRUCT *_STACK;

_STACK _STACK_create(size_t size_single_element);
void _STACK_copy(_STACK stack, void *array_to_copy, size_t array_size);
_Bool _STACK_is_empty(_STACK stack);
_Bool _STACK_push(_STACK stack, void *element, size_t size_element);
void* _STACK_pop(_STACK stack);
void _STACK_free(_STACK stack);

#endif
