#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include "_SET.h"
#include "_LIST.h"

struct _SET_STRUCT
{
	_LIST list;
};

/*The function _SET_create returns the pointer to a _SET.
 * Returns:
 * the pointer to the set if succesful;
 * NULL if the element size is <=0, the compare function is NULL, or if the necessary allocations weren't succesful.
 * Parameters:
 * size_t element_size: the size of the elements inside the set;
 * int (*compare)(void *a1, void a2): the compare function.
 */

_SET _SET_create(size_t element_size, int (*compare)(void *a1, void *a2))
{
	_SET set;
	if(compare==NULL || element_size<=0)
		return NULL;
	set=malloc(sizeof(struct _SET_STRUCT));
	if(set==NULL)
		return NULL;
	set->list=_LIST_create(element_size, compare, NULL);
	if(set->list==NULL)
	{
		free(set);
		return NULL;
	}
	return set;
}

/*The function _SET_contains checks if the set contains the element in the parameters.
 * Returns:
 * 1 if the set contains the element in the parameters;
 * 0 if the set DOES NOT contain the element in the parameters;
 * -1 if the function couldn't check (set doesn't exist, element doesn't exist..)
 * Parameters:
 * _SET set: the set to operate on;
 * void *element: the element to search.
 */

int _SET_contains(_SET set, void *element)
{
	int flag;
	if(set==NULL || set->list==NULL || element==NULL)
		return -1;
	flag=_LIST_search(set->list,element);
	if(flag>=0)
		return 1;
	else if(flag==-1)
		return 0;
	else
		return -1;
}

/*The function _SET_add adds the element in the parameters to the set.
 * Returns:
 * 1 if the insertion was succesful.
 * 0 if the insertion wasn't succesful, the element is illegal.
 * Parameters:
 * _SET set: the set to work on;
 * void *element: the element to add to the set;
 * size_t element_size: the size of the element to add.
 */

_Bool _SET_add(_SET set, void *element, size_t element_size)
{
	if(set==NULL || set->list==NULL || element==NULL)
		return 0;
	if(_SET_contains(set,element)==0)
		if(_LIST_add(set->list,element,element_size)==1)
			return 1;
	return 0;
}

/*The function _SET_remove removes the element in the parameters.
 * Returns:
 * 1 if the deletion was succesful;
 * 0 if deletion wasn't succesful.
 * Parameters:
 * _SET set: the set to work on;
 * void *element: the element requested.
 */

_Bool _SET_remove(_SET set, void *element)
{
	int flag;
	if(set==NULL || set->list==NULL || element==NULL)
		return 0;
	flag=_LIST_search_destroy(set->list,element);
	if(flag==1)
		return 1;
	return 0;
}

/*The _SET_free frees the memory allocated for the set.
 * Returns:
 * 1 in case of succesful deletion;
 * 0 otherwise.
 * Parameters:
 * _SET set: the set to free.
 */

_Bool _SET_free(_SET set)
{
	if(set==NULL)
		return 0;
	if(set->list!=NULL)
		_LIST_free(set->list);
	free(set);
	return 1;
}
