#ifndef _SET_H
#define _SET_H 1
#include <stddef.h>

typedef struct _SET_STRUCT *_SET;

_SET _SET_create(size_t element_size, int (*compare)(void *a1, void *a2));
int _SET_contains(_SET set,void *element);
_Bool _SET_add(_SET set, void *element, size_t element_size);
_Bool _SET_remove(_SET set, void *element);
_Bool _SET_free(_SET set);

#endif

