# YokoGenericDataStructures

This is a C99 implementation of commonly used data structures supporting generic data types.

---

## Features

The library currently implements:

- Stack
- List
- Set
- Queue

Additionally, provided the comparing function, sorted data structures are also available.<br>
Currently, the library implements:

- Sorted List
- Sorted Set

---

## How to use it
Just include the headers in your file. Use the .c files as normal source files.<br>
All the functions are documented in the .c files, make sure to read the comments if you need to understand how to use them.<br>
Please remember that many data structures rely on the List implementation, so you may need to include the list files for your program to compile.<br>

---

## Possible issues with _Bool
The library uses _Bool type introduced in C99 standard. If the compiler of your choice does not implement this data type, try substituting with a similar data type like int or char.<br>

---

## License
Please refer to the license file inside the repository.<br>
