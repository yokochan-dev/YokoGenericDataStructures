#ifndef _QUEUE_H
#define _QUEUE_H 1
#include <stddef.h>

typedef struct _QUEUE *_QUEUE;

_QUEUE _QUEUE_create(size_t item_size, void (*print)(void *a));
_Bool _QUEUE_enqueue(_QUEUE q, void *item, size_t item_size);
void* _QUEUE_dequeue(_QUEUE q);
void* _QUEUE_peek(_QUEUE q);
_Bool _QUEUE_destroy(_QUEUE q);
void _QUEUE_print(_QUEUE q);

#endif
