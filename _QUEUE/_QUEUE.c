#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include "_QUEUE.h"
#include "_LIST.h"

struct _QUEUE
{
    _LIST list;
};

/*The function _QUEUE_create creates the queue. The list is initialized to NULL.
 * Returns:
 * pointer to queue if succesful;
 * NULL if unsuccesful.
 * Parameters:
 * size_t item_size: size of the items composing the queue.
 */

_QUEUE _QUEUE_create(size_t item_size, void (*print)(void *))
{
    _QUEUE q;
    if(item_size<=0 || print==NULL)
        return NULL;
    q=(_QUEUE)malloc(sizeof(struct _QUEUE));
    if(q==NULL)
        return NULL;
    q->list=_LIST_create(item_size, NULL, print);
    if(q->list==NULL)
    {
        free(q);
        return NULL;
    }
    return q;
}

/*The function _QUEUE_enqueue enqueues an element to the queue (tail).
 * The function makes a copy of the element.
 * Returns:
 * 1 if insertion if succesful;
 * 0 if unsuccesful, or the queue doesn't exist.
 * Parameters:
 * _QUEUE q: the queue to operate on;
 * void *item: the item to enqueue;
 * size_t item_size: the size of the item to enqueue.
 */

_Bool _QUEUE_enqueue(_QUEUE q, void *item, size_t item_size)
{
    if(q==NULL)
		return 0;
    if(_LIST_add(q->list,item,item_size)==0)
      return 0;
    return 1;
}

/*The function _QUEUE_dequeue returns and removes the element at the head. Client doesn't allocate, but must destroy.
 * The function returns a copy of the element dequeued.
 * Returns:
 * pointer to element if succesful;
 * NULL if unsuccesful, list or queue don't exist.
 * Parameters:
 * _QUEUE q: the queue to operate on.
 */

void* _QUEUE_dequeue(_QUEUE q)
{
    void *tmp;
    if(q==NULL || q->list==NULL)
        return NULL;
    if((tmp=_LIST_search_destroy_by_index(q->list,0))==NULL)
        return NULL;
    return tmp;
}

/*The function _QUEUE_peek returns but DOES NOT REMOVE the element at the head. Client doesn't allocate, but must destroy.
 * The function returns a pointer to the element itself.
 * Returns:
 * pointer to element if succesful;
 * NULL if unsuccesful, list or queue don't exist.
 * Parameters:
 * _QUEUE q: the queue to operate on.
 */

void* _QUEUE_peek(_QUEUE q)
{
    void *tmp;
    if(q==NULL || q->list==NULL)
        return NULL;
    if((tmp=_LIST_search_by_index(q->list,0))==NULL)
        return NULL;
    return tmp;
}

/*The function _QUEUE_destroy frees all the memory allocated by the queue.
 * Returns:
 * 1 if succesful;
 * 0 if unsuccesful.
 * Parameters:
 * _QUEUE q: the queue to operate on.
 */

_Bool _QUEUE_destroy(_QUEUE q)
{
    if(q==NULL)
        return 0;
    _LIST_free(q->list);
    free(q);
    return 1;
}

void _QUEUE_print(_QUEUE q)
{
	if(q==NULL || q->list==NULL || q->list->print==NULL)
		return;
	_LIST_print(q->list);
	return;
}
