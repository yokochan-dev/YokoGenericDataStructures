#ifndef _SORTED_SET_H
#define _SORTED_SET_H 1
#include <stddef.h>

typedef struct _SORTED_SET_STRUCT *_SORTED_SET;

_SORTED_SET _SORTED_SET_create(size_t element_size, int (*compare)(void *a1, void *a2));
int _SORTED_SET_contains(_SORTED_SET set,void *element);
_Bool _SORTED_SET_add(_SORTED_SET set, void *element, size_t element_size);
_Bool _SORTED_SET_remove(_SORTED_SET set, void *element);
_Bool _SORTED_SET_free(_SORTED_SET set);

#endif
