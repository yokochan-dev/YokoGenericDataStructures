#ifndef _SORTED_LIST_H
#define _SORTED_LIST_H 1
#include <stddef.h>

typedef struct _SORTED_LIST_STRUCT *_SORTED_LIST;

_SORTED_LIST _SORTED_LIST_create(size_t element_size, int (*compare)(void *a1, void *a2), void (*print)(void *a));
_Bool _SORTED_LIST_add(_SORTED_LIST list, void *item, size_t size_item);
int _SORTED_LIST_search(_SORTED_LIST list, void *item);
int _SORTED_LIST_search_destroy(_SORTED_LIST list,void *item);
void *_SORTED_LIST_search_by_index(_SORTED_LIST list, int index);
void *_SORTED_LIST_search_destroy_by_index(_SORTED_LIST list, int index);
_Bool _SORTED_LIST_free(_SORTED_LIST list);
_Bool _SORTED_LIST_print(_SORTED_LIST list);

#endif
